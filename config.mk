PRODUCT_PACKAGES += \
    CameraPicProcService \
    OnePlusCameraService \
    oneplus-framework-res \
    seccamservice

PRODUCT_COPY_FILES += \
    $(LOCAL_PATH)/system/etc/sysconfig/hiddenapi-package-whitelist-oneplus.xml:$(TARGET_COPY_OUT_SYSTEM)/etc/sysconfig/hiddenapi-package-whitelist-oneplus.xml
